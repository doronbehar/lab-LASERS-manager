# Since our target wavelengths are at the regime of ~935nm or so, and we print
# the frequencies also in THz, it is better to use the speed of light in units
# of nm*THz, to make python floating point calculations more accurate and
# faster
SPEED_OF_LIGHT = 299792.458*1e12
UNITSFACTOR = {
    # Relative to Hz
    'KHz': 1e3,
    'MHz': 1e6,
    'GHz': 1e9
}

