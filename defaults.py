import os
import sys

import numpy as np

sys.path.append(os.path.dirname(__file__))
from constants import SPEED_OF_LIGHT

# To add channels to the GUI, simply add a line to end of each of the following
# arrays.
TARGET_FREQS = SPEED_OF_LIGHT/np.array([
    780,
    935,
    399,
])

TARGET_WAVELENGTHS_GUI_COLORS = [
    (200, 0,   0),
    (100, 0,   0),
    (0  , 100, 0),
]

TARGET_FREQS_DB_FILE = os.environ['HOMEPATH' if os.name == "nt" else 'HOME'] \
    + os.path.sep \
    + ".LASERS-manager.h5"

# Other path observed in some installations:
#
# C:\Program Files (x86)\HighFinesse\Wavelength Meter WS7 4626\Projects\wlmData.dll
# C:\Windows\System32\wlmDataws7.dll
#
# TODO: Maybe try to find the wlmData.dll file in other locations if it doesn't
# exist?
WLMDATA_PATH = r'C:\Windows\System32\wlmData.dll'

LASER_FREQS_BUFFER_SIZE = 10000
# In seconds
LASER_FREQS_BUFFER_UPDATE_INTERVAL = 0.0001

# Here we want a bit more history, and we don't need to sample the data so fast
# as with the frequncies buffers.
LDC_CURRENT_BUFFER_SIZE = 100000
LDC_CURRENT_BUFFER_UPDATE_INTERVAL = 0.001
LDC_IP = "10.10.68.48"
