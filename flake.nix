{
  description = "Flake for our lab's software based PID locked LASERS (part of my Master's thesis)";

  # To make user overrides of the nixpkgs flake not take effect
  # Using my fork until https://github.com/NixOS/nixpkgs/pull/327446 is merged
  inputs.nixpkgs.url = "github:doronbehar/nixpkgs/pkg/PlotPyStack";

  outputs = { self
  , nixpkgs
  }: let
    pkgs = import nixpkgs {
      system = "x86_64-linux";
      overlays = [
      ];
      config = {
        allowUnfree = true;
      };
    };
    pyPkgs = p: [
      p.numpy
      p.qwt
      p.qtpy
      p.pyqt6
      p.h5py
      # For text editor
      p.jedi-language-server
      p.debugpy
      p.black
    ];
    pythonEnv = pkgs.python3.withPackages pyPkgs;
  in {
    devShell.x86_64-linux = pkgs.mkShell {
      nativeBuildInputs = [
        pythonEnv
        pkgs.qtcreator
      ];
      # Not needed to launch the tool, but needed for qtcreator
      buildInputs = [
        pkgs.qt6.qtbase
        pkgs.qt6.qtwayland
      ];
      QT_PLUGIN_PATH = pkgs.lib.pipe [
        pkgs.qt6.qtbase
        pkgs.qt6.qtwayland
      ] [
        (map (p: "${pkgs.lib.getBin p}/${pkgs.qt6.qtbase.qtPluginPrefix}"))
        (pkgs.lib.concatStringsSep ":")
      ];
      QT_QPA_PLATFORM="wayland";
      XDG_DATA_DIRS = pkgs.lib.concatStringsSep ":" [
        # So we'll be able to save figures from the plot dialog
        "${pkgs.gsettings-desktop-schemas}/share/gsettings-schemas/${pkgs.gsettings-desktop-schemas.name}"
        "${pkgs.gtk3}/share/gsettings-schemas/${pkgs.gtk3.name}"
        # So pdf mime types and perhaps others could be detected by 'gio open'
        # / xdg-open. TODO: Is this the best way to overcome this issue -
        # manually every time we generate a devShell?
        "${pkgs.shared-mime-info}/share"
        "${pkgs.hicolor-icon-theme}/share"
      ];
      GDK_PIXBUF_MODULE_FILE = "${pkgs.librsvg}/${pkgs.gdk-pixbuf.moduleDir}.cache";
    };
    packages.x86_64-linux = {
      inherit
        pythonEnv
      ;
    };
  };
}
