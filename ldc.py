#!/usr/bin/env python

import socket
import random
import argparse
import sys
import os

import numpy as np

sys.path.append(os.path.dirname(__file__))
from defaults import LDC_IP

class LDCConnectionError(ConnectionError):
    pass

class LDC:
    def __init__(self, host=LDC_IP, port=8888):
        self.s = socket.socket()
        self.s.settimeout(2)
        self.s.connect((host, port))
        if port == 8886:
            # Recieve the "welcome" string
            print(">", self.recv(4096*4))
        self.send("ULOC 1")
        self.send("*IDN?")
        # Needs a larger buffer size because it is a long string
        self.id = self.recv(4096*6)
        if port == 8886:
            print("Identity is:", self.id)
        self.send("SILM?")
        self.current_limit = float(self.recv())
        # TODO: Make sure it is in CC mode, because otherwise we can't control
        # the current. Raise an error if that is the case
    # Wrapper around self.s.send, that also adds the Carriage Return ("\r")
    def send(self, data):
        return self.s.send((data + "\r").encode())
    # The default of 16 is indeed very low - but it fits all self.s.recv calls
    # that expect a float.
    def recv(self, bufsize=16):
        return self.s.recv(bufsize).decode()
    @property
    def current(self):
        self.send("RILD?")
        return float(self.recv())
    @current.setter
    def current(self, current):
        if current >= self.current_limit:
            raise ValueError(
                "Can't raise current above the limit of {}mA".format(limit)
            )
        self.send("SILD {}".format(current))
        # TODO: Report if this was successful?

class LDCshim:
    def __init__(self, host, port=8886, current_limit=np.inf):
        self.id = "Stanford_Research_Systems,LDC501,s/n098163,ver2.02"
        if port == 8886:
            # Fake a welcome message if connecting to the debug server, like in
            # class LDC.
            print("> 220 Welcome DBG server!")
        if port == 8886:
            print("Identity is (fake): {}".format(self.id))
        self._current = random.uniform(95, 105)
        self.current_limit = current_limit
    @property
    def current(self):
        # In real life the LDC reports a slightly different current from the
        # one you request.
        return self._current + random.uniform(-0.01, 0.01)
    @current.setter
    def current(self, current):
        if current >= self.current_limit:
            raise ValueError(
                "Can't raise current above the limit of {}mA".format(limit)
            )
        self._current = current

if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Test our LDC 500 connection and Python class via a Python "
        " debugger"
    )
    parser.add_argument(
        "--ip",
        help="IP addresss of the LDC 500",
        default=[LDC_IP],
        type=str,
        nargs=1,
    )
    parser.add_argument(
        "--port",
        help="Port of the LDC 500 - use 8888 for not using the debug LDC server"
             " (see manual...)",
        default=[8886],
        type=int,
        nargs=1,
    )
    args = parser.parse_args()
    ldc = LDC(
        host=args.ip[0],
        port=args.port[0],
    )
    __import__('pdb').set_trace()
