#!/usr/bin/env python

import sys
import time
from datetime import timedelta
import os
from itertools import combinations_with_replacement
from itertools import count
from math import copysign
from collections import deque
import argparse
from functools import partial

import numpy as np
import h5py
from qtpy import (
    QtWidgets,
    QtGui,
    QtCore,
)
import qwt

sys.path.append(os.path.dirname(__file__))
from wlm import WavelengthMeter, WavelengthMeterShim, WLMConnectionError
from ldc import LDC, LDCshim, LDCConnectionError
import defaults
from constants import SPEED_OF_LIGHT, UNITSFACTOR

class FreqMeasurer(QtCore.QRunnable):
    def __init__(self, pretendWlm, dllpath, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if pretendWlm >= 0:
            self.wlm = WavelengthMeterShim(randomRange=pretendWlm)
        else:
            self.wlm = WavelengthMeter(dllpath)
            # May raise a WLMConnectionError which will be caught by
            # ApplicationWindow's __init__, we don't care about the return
            # value at this point.
            self.wlm.GetFrequency(1)
        self.underExposedChannels = set()
        # reducing by 1 second so that self.updateUptime won't miss the correct
        # minute by 1 second, due to the int() rounding.
        self.initTime = time.time() - 1
        self.timeBuffer = deque(maxlen=defaults.LASER_FREQS_BUFFER_SIZE)
        self.measurementBuffers = [deque(maxlen=defaults.LASER_FREQS_BUFFER_SIZE) for _ in defaults.TARGET_FREQS]
        self.targetBuffers = [deque(maxlen=defaults.LASER_FREQS_BUFFER_SIZE) for _ in defaults.TARGET_FREQS]
        self.keepRun = True
    def getTargetFreq(self, channel):
        # This attribute is set by ApplicationWindow
        return self.targetFreqWidgets[channel].thzLCD.value()
    def run(self):
        while self.keepRun:
            # Don't exhaust the DLL and the GUI
            time.sleep(defaults.LASER_FREQS_BUFFER_UPDATE_INTERVAL)
            # We are don't care about time difference between channels
            self.timeBuffer.append(time.time() - self.initTime)
            for channel,_ in enumerate(defaults.TARGET_FREQS):
                # Channels in wlm are indexed from 1 - like the physical labels
                # on the switcher
                f = self.wlm.GetFrequency(channel+1)
                if np.isnan(f):
                    self.underExposedChannels.add(channel+1)
                self.measurementBuffers[channel].append(f)
                self.targetBuffers[channel].append(
                    self.getTargetFreq(channel)
                )

class WavelengthsPlot(qwt.QwtPlot):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.setCanvasBackground(QtCore.Qt.white)
        self.setAxisTitle(qwt.QwtPlot.xBottom, "Time [s]")
        self.setAxisTitle(qwt.QwtPlot.yLeft, "Frequency [Hz]")
        self.timeBuffer = deque(maxlen=defaults.LASER_FREQS_BUFFER_SIZE)
        self.measurementCurve = qwt.QwtPlotCurve("Measurement")
        self.measurementCurve.attach(self)
        self.measurementCurve.setPen(QtGui.QPen(QtCore.Qt.green))
        self.targetCurve = qwt.QwtPlotCurve("Target")
        self.targetCurve.attach(self)
        self.targetCurve.setPen(QtGui.QPen(QtCore.Qt.blue))
        # Makes the target line always be "above" the "layer" of the
        # measurement line. Credit:
        # https://stackoverflow.com/a/66667445/4935114
        # TODO: Fix this
        #self.targetCurve.setZValue(1)
        # TODO: Set a relative, smart formatter like was with matplotlib -
        # with offset calculation etc.

class LdcCurrentMeasurer(QtCore.QRunnable):
    def __init__(self, pretend, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if pretend:
            self.wl935ldc = LDCshim()
        else:
            # TODO: Consider exposing these options to the CLI
            #
            # May raise a LDCConnectionError which will be caught by
            # `ApplicationWindow's __init__`.
            self.wl935ldc = LDC()
        self.initTime = time.time() - 1
        self.timeBuffer = deque(maxlen=defaults.LDC_CURRENT_BUFFER_SIZE)
        self.currentBuffer = deque(maxlen=defaults.LDC_CURRENT_BUFFER_SIZE)
        self.keepRun = True
    def run(self):
        while self.keepRun:
            # Don't exhaust the socket and the GUI
            time.sleep(defaults.LDC_CURRENT_BUFFER_UPDATE_INTERVAL)
            # We are don't care about time difference between channels
            self.timeBuffer.append(time.time() - self.initTime)
            self.currentBuffer.append(self.wl935ldc.current)

class LdcCurrentPlot(qwt.QwtPlot):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.setCanvasBackground(QtCore.Qt.white)
        self.setAxisTitle(qwt.QwtPlot.xBottom, "Time [s]")
        self.setAxisTitle(qwt.QwtPlot.yLeft, "Current [mA]")
        self.curve = qwt.QwtPlotCurve("Current")
        self.curve.setPen(QtGui.QPen(QtCore.Qt.red))
        self.curve.attach(self)

class TargetFreqWidget(QtWidgets.QWidget):
    units = [
        "nm",
        "GHz",
        "MHz",
        "KHz",
    ]
    def __init__(self, freq, rgbColor, comboBoxPreset):
        super().__init__()
        self.setStyleSheet(
            "QLCDNumber {{ background-color: rgb({});color: rgb({}); }}".format(
                ",".join(np.array(rgbColor).astype(str)),
                # Inverse color - to make sure it is readable
                ",".join(
                    (np.full_like(rgbColor, 255) - rgbColor).astype(str)
                )
            )
        )
        layout = QtWidgets.QGridLayout(self)
        # Handle the changing widgets
        changeFreqLabel = QtWidgets.QLabel()
        changeFreqLabel.setText("Change target wavelength:")
        layout.addWidget(changeFreqLabel, 0, 0)
        self.spinBox = QtWidgets.QSpinBox()
        self.spinBox.setMinimum(-1000)
        self.spinBox.setMaximum(+1000)
        layout.addWidget(self.spinBox, 0, 1)
        self.resetButton = QtWidgets.QPushButton("Reset View")
        layout.addWidget(self.resetButton, 0, 2)
        self.comboBox = QtWidgets.QComboBox()
        for unitIdx,unit in enumerate(self.units):
            self.comboBox.addItem(unit)
            if comboBoxPreset == unit:
                self.comboBox.setCurrentIndex(unitIdx)
        layout.addWidget(self.comboBox, 0, 3)
        # Handle the display widgets
        currentFreqLabel = QtWidgets.QLabel()
        currentFreqLabel.setText("Current target wavelength:")
        layout.addWidget(currentFreqLabel, 1, 0)
        self.nmLCD = QtWidgets.QLCDNumber()
        self.nmLCD.smallDecimalPoint = True
        self.nmLCD.setDigitCount(15)
        self.nmLCD.display(SPEED_OF_LIGHT/freq)
        nmLCDUnitsLabel = QtWidgets.QLabel()
        nmLCDUnitsLabel.setText("nm")
        nmLCDwrapper = QtWidgets.QWidget()
        nmLCDwrapperLayout = QtWidgets.QHBoxLayout(nmLCDwrapper)
        nmLCDwrapperLayout.addWidget(self.nmLCD)
        nmLCDwrapperLayout.addWidget(nmLCDUnitsLabel)
        layout.addWidget(nmLCDwrapper, 1, 1,1,2)
        self.thzLCD = QtWidgets.QLCDNumber()
        self.thzLCD.smallDecimalPoint = True
        self.thzLCD.setDigitCount(15)
        self.thzLCD.display(freq)
        thzLCDUnitsLabel = QtWidgets.QLabel()
        thzLCDUnitsLabel.setText("THz")
        thzLCDwrapper = QtWidgets.QWidget()
        thzLCDwrapperLayout = QtWidgets.QHBoxLayout(thzLCDwrapper)
        thzLCDwrapperLayout.addWidget(self.thzLCD)
        thzLCDwrapperLayout.addWidget(thzLCDUnitsLabel)
        layout.addWidget(thzLCDwrapper, 1, 3)

class ApplicationWindow(QtWidgets.QMainWindow):
    def __init__(
        self,
        targetsDbFname=defaults.TARGET_FREQS_DB_FILE,
        dllpath=defaults.WLMDATA_PATH,
        pretendWlm=-1,
    ):
        super().__init__()
        self.setWindowTitle("LASERS manager")
        self.setWindowIcon(QtGui.QIcon(
            os.path.dirname(os.path.realpath(__file__)) + \
            os.path.sep + \
            # From:
            # https://commons.wikimedia.org/wiki/File:Spade_laser_incrociate.svg
            # TODO: It would have been ideal to use the svg
            'logo.png'
        ))
        try:
            self.freqMeasurer = FreqMeasurer(
                pretendWlm,
                dllpath=dllpath,
            )
            self.ldcMeasurer = LdcCurrentMeasurer(
                pretend=bool(pretendWlm >=0)
            )
        except Exception as e:
            self._main = QtWidgets.QLabel()
            self.setCentralWidget(self._main)
            self._main.setTextFormat(QtCore.Qt.TextFormat.RichText)
            self._main.setTextInteractionFlags(
                QtCore.Qt.TextInteractionFlag.TextSelectableByMouse
            )
            redError = "<h2><span style='color: red;'>ERROR:</span>"
            if isinstance(e, FileNotFoundError):
                mainError = " DLL path does not exist:"
                moreInfo = dllpath
            elif isinstance(e, WLMConnectionError):
                mainError = "Wavelength meter is not connected!"
                moreInfo = ""
            elif isinstance(e, LDCConnectionError):
                mainError = "Laser Diode Controller is not connected!"
                moreInfo = str(e)
            else:
                mainError = " Unexpected error:"
                moreInfo = str(e)
            # TODO: Fix monospace font...
            afterH2 = "</h2><pre style='font-family: Courier New;'>"
            postPre = "</pre>"
            self._main.setText(
                redError + mainError + afterH2 + moreInfo + postPre
            )
            self.exitCode = 2
            return
        self.exitCode = 0
        self._main = QtWidgets.QWidget()
        self.setCentralWidget(self._main)
        self.tabs = QtWidgets.QTabWidget(self._main)
        self.wlmTab = QtWidgets.QWidget()
        self.tabs.addTab(self.wlmTab, "Wavelengths")
        self.uptimeWidget = QtWidgets.QLabel(self.wlmTab)
        uptimeTimer = QtCore.QTimer(self._main)
        uptimeTimer.setInterval(
            # Every minute
            1000*60
        )
        uptimeTimer.timeout.connect(self.updateUptime)
        uptimeTimer.start()
        self.updateUptime()
        self.underExposedWidget = QtWidgets.QLabel(self.wlmTab)
        geometry = QtWidgets.QApplication.instance().primaryScreen().availableGeometry()
        layout = QtWidgets.QGridLayout(self.wlmTab)
        layout.addWidget(self.uptimeWidget, 0,0)
        layout.addWidget(self.underExposedWidget, 0, 1)
        self.config = h5py.File(
            targetsDbFname,
            "a"
        )
        if len(self.config.keys()) == 0:
            self.config.create_dataset(
                "freqs",
                data=defaults.TARGET_FREQS,
                # Enables it to be resized
                chunks=True,
                maxshape=(None,)
            )
            self.config.create_dataset(
                "scales",
                data=["KHz"]*len(defaults.TARGET_FREQS),
                # Enables it to be resized
                chunks=True,
                maxshape=(None,)
            )
        # Make sure all freqs in defaults.TARGET_FREQS are handled - in case
        # someone decides to add  a new LASER to
        self.config['freqs'].resize(
            (len(defaults.TARGET_FREQS),)
        )
        self.config['scales'].resize(
            (len(defaults.TARGET_FREQS),)
        )
        #### Lists per laser
        self.freqMeasurer.targetFreqWidgets = [None] * len(self.config['freqs'])
        self.plots = [None] * len(self.config['freqs'])
        ####
        for (channel,freq),rowCol in zip(
            enumerate(self.config['freqs']),
            # We have 9 channels altogether, so we create a grid of maximal 3x3
            # size, and we generate the row and column indices here:
            combinations_with_replacement(range(3),2)
        ):
            # Initialize value if needed (if the resize actually did something)
            if np.isclose(freq, 0):
                self.config['freqs'][channel] = defaults.TARGET_FREQS[channel]
            self.freqMeasurer.targetFreqWidgets[channel] = TargetFreqWidget(
                freq,
                rgbColor=defaults.TARGET_WAVELENGTHS_GUI_COLORS[channel],
                comboBoxPreset=self.config['scales'][channel].decode(),
            )
            self.freqMeasurer.targetFreqWidgets[channel].setMaximumWidth(
                int((geometry.width()-30)/3),
            )
            self.plots[channel] = WavelengthsPlot(self.wlmTab)
            self.plots[channel].setMinimumHeight(
                int((geometry.height()-100)/3),
            )
            self.freqMeasurer.targetFreqWidgets[channel].resetButton.clicked.connect(
                # https://stackoverflow.com/a/39606008/4935114
                partial(self.resetBuffers, channel)
            )

            # Actually, its not a 3x3 grid, but a 6x3 grid - every laser has 2
            # widgets - canvas, a TargetFreqWidget, and we have the uptimeWidget at
            # the top as well
            layout.addWidget(
                self.plots[channel],
                rowCol[0]*3+1,
                rowCol[1],
            )
            layout.addWidget(
                self.freqMeasurer.targetFreqWidgets[channel],
                rowCol[0]*3+2,
                rowCol[1],
            )
        ldcTab = QtWidgets.QWidget()
        self.ldcCurrentPlot = LdcCurrentPlot(ldcTab)
        # TODO: Show in a text widget the ldc of self.ldcMeasurer.wl935ldc.identity
        self.ldcCurrentPlot.setMinimumHeight(
            int((geometry.height()-100)/3),
        )
        self.ldcCurrentPlot.setMinimumWidth(
            int((geometry.width()-100)/3),
        )
        self.tabs.addTab(ldcTab, "LDC 935nm")
        # After all elements are placed, we can set a width and height for the
        # whole application
        self.resize(
            geometry.width(),
            # TODO: It would have been nice if there was a less guessing way to
            # get that 160 integer - try to use sizeHint
            int((geometry.height()+160)/rowCol[1])
        )
        # 50 Hz frequency plot update - fast enough so it will appear smooth,
        # but not too fast for the GUI's event loop to be overloaded with
        # requests for updates.
        self.startTimer(25)
        self.threadpool = QtCore.QThreadPool()
        self.threadpool.start(self.freqMeasurer)
        self.threadpool.start(self.ldcMeasurer)

    def timerEvent(self, e):
        self.ldcCurrentPlot.curve.setData(
            list(self.ldcMeasurer.timeBuffer),
            list(self.ldcMeasurer.currentBuffer),
        )
        self.ldcCurrentPlot.replot()
        # If file is closed, do nothing
        if not self.config:
            return
        for channel,plot in enumerate(self.plots):
            plot.targetCurve.setData(
                list(self.freqMeasurer.timeBuffer),
                list(self.freqMeasurer.targetBuffers[channel]),
            )
            self.underExposedWidget.setText(
                "Under exposed channels: {}".format(
                    self.freqMeasurer.underExposedChannels
                ) if self.freqMeasurer.underExposedChannels else "")
            plot.measurementCurve.setData(
                list(self.freqMeasurer.timeBuffer),
                list(self.freqMeasurer.measurementBuffers[channel]),
            )
            plot.replot()
    def resetBuffers(self, channel):
        self.freqMeasurer.timeBuffer.clear()
        self.freqMeasurer.targetBuffers[channel].clear()
        self.freqMeasurer.measurementBuffers[channel].clear()
    def updateUptime(self):
        delta = timedelta(seconds=time.time()-self.freqMeasurer.initTime)
        self.uptimeWidget.setText("Uptime: {}d + {}h + {}m".format(
            delta.days, int(delta.seconds/60/60), int(delta.seconds/60)
        ))

    def keyPressEvent(self, event):
        # Maybe the case if we encountered a startup error
        if not hasattr(self, 'measurer'):
            return
        for channel,targetFreqW in enumerate(self.freqMeasurer.targetFreqWidgets):
            if QtWidgets.QApplication.focusWidget() in [
                targetFreqW.comboBox,
                targetFreqW.spinBox
            ] and event.key() == QtCore.Qt.Key.Key_Return:
                self.targetFreqUpdate(
                    channel,
                    targetFreqW.spinBox.value(),
                    targetFreqW.comboBox.currentText()
                )
                # Reset the spinBox
                targetFreqW.spinBox.setValue(0)
                # No need to keep iterate the other widgets
                break

    def targetFreqUpdate(self, channel, addition, unit):
        if unit == "nm":
            newFreq = SPEED_OF_LIGHT/(
                (SPEED_OF_LIGHT/self.freqMeasurer.getTargetFreq(channel)) \
                + addition
            )
        else:
            newFreq = self.freqMeasurer.getTargetFreq(channel) + addition * UNITSFACTOR[unit]
        self.freqMeasurer.targetFreqWidgets[channel].nmLCD.display(SPEED_OF_LIGHT/newFreq)
        self.freqMeasurer.targetFreqWidgets[channel].thzLCD.display(newFreq)
        self.config['scales'][channel] = unit
        self.config['freqs'][channel] = newFreq
        # update the file constantly, so the file will have the correct value
        # for external programs that run in the background.
        self.config.flush()
    def closeEvent(self, event):
        # The attribute may not exist if we encountered a startup errors
        if hasattr(self, 'freqMeasurer'):
            self.freqMeasurer.keepRun = False
        if hasattr(self, 'ldcMeasurer'):
            self.ldcMeasurer.keepRun = False
        # The attribute may not exist if we encountered startup errors
        if hasattr(self, 'config'):
            self.config.close()

if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Show and manage our lab's LASERs' frequencies"
    )
    parser.add_argument(
        "-t", "--targetsDb",
        help="Path to the .h5 database in which the previous' session's target"
        " frequencies were saved, defaults to: " + defaults.TARGET_FREQS_DB_FILE,
        default=[defaults.TARGET_FREQS_DB_FILE],
        type=str,
        nargs=1,
    )
    parser.add_argument(
        "--wlmData",
        help="Path to the .dll file of the wavelength meter, defaults to: " + \
            defaults.WLMDATA_PATH,
        default=[defaults.WLMDATA_PATH],
        type=str,
        nargs=1,
    )
    parser.add_argument(
        "--pretendWlm",
        help="Pretend there is a WLM connected - useful when testing the GUI",
        nargs=1,
        type=float,
        default=[-1],
        metavar="NOISE(KHz)",
    )
    args = parser.parse_args()
    # Check whether there is already a running QApplication (e.g., if running
    # from an IDE).
    qapp = QtWidgets.QApplication.instance()
    if not qapp:
        qapp = QtWidgets.QApplication(sys.argv)

    app = ApplicationWindow(
        targetsDbFname=args.targetsDb[0],
        dllpath=args.wlmData[0],
        pretendWlm=args.pretendWlm[0],
    )
    app.show()
    app.activateWindow()
    app.raise_()
    qapp.exec()
    sys.exit(app.exitCode)
