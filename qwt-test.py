#!/usr/bin/env python

import random
import time

import numpy as np
from qtpy.QtCore import QSize, Qt
from qtpy.QtGui import QBrush, QPen
from qtpy import QtWidgets

from qwt import QwtPlot, QwtPlotCurve, QwtSymbol
from qwt.tests import utils


def standard_map(x, y, kappa):
    """provide one interate of the inital conditions (x, y)
    for the standard map with parameter kappa."""
    y_new = y - kappa * np.sin(2.0 * np.pi * x)
    x_new = x + y_new
    # bring back to [0,1.0]^2
    if (x_new > 1.0) or (x_new < 0.0):
        x_new = x_new - np.floor(x_new)
    if (y_new > 1.0) or (y_new < 0.0):
        y_new = y_new - np.floor(y_new)
    return x_new, y_new


class MapDemo(QtWidgets.QMainWindow):
    def __init__(self, *args):
        QtWidgets.QMainWindow.__init__(self, *args)
        self._main = QtWidgets.QWidget()
        self.setCentralWidget(self._main)
        layout = QtWidgets.QVBoxLayout(self._main)
        self.plot1 = QwtPlot(self)
        self.plot1.setTitle("A Simple Map Demonstration, 1")
        self.plot1.setCanvasBackground(Qt.white)
        self.plot1.setAxisTitle(QwtPlot.xBottom, "x")
        self.plot1.setAxisTitle(QwtPlot.yLeft, "y")
        self.plot1.setAxisScale(QwtPlot.xBottom, 0.0, 1.0)
        self.plot1.setAxisScale(QwtPlot.yLeft, 0.0, 1.0)
        self.plot2 = QwtPlot(self)
        self.plot2.setTitle("A Simple Map Demonstration, 2")
        self.plot2.setCanvasBackground(Qt.white)
        self.plot2.setAxisTitle(QwtPlot.xBottom, "x")
        self.plot2.setAxisTitle(QwtPlot.yLeft, "y")
        self.plot2.setAxisScale(QwtPlot.xBottom, 0.0, 1.0)
        self.plot2.setAxisScale(QwtPlot.yLeft, 0.0, 1.0)
        layout.addWidget(self.plot1)
        layout.addWidget(self.plot2)
        # Initialize map data
        self.count = self.i = 1000
        self.xs1 = np.zeros(self.count, float)
        self.ys1 = np.zeros(self.count, float)
        self.xs2 = np.zeros(self.count, float)
        self.ys2 = np.zeros(self.count, float)
        self.kappa = 0.2
        self.curve1 = QwtPlotCurve("Map")
        self.curve1.attach(self.plot1)
        self.curve1.setSymbol(
            QwtSymbol(QwtSymbol.Ellipse, QBrush(Qt.red), QPen(Qt.blue), QSize(5, 5))
        )
        self.curve1.setPen(QPen(Qt.cyan))
        self.curve2 = QwtPlotCurve("Map")
        self.curve2.attach(self.plot2)
        self.curve2.setSymbol(
            QwtSymbol(QwtSymbol.Ellipse, QBrush(Qt.red), QPen(Qt.blue), QSize(5, 5))
        )
        self.curve2.setPen(QPen(Qt.cyan))
        # 1 tick = 1 ms, 10 ticks = 10 ms (Linux clock is 100 Hz)
        self.ticks = 1
        self.tid = self.startTimer(self.ticks)
        self.timer_tic = None
        self.user_tic = None
        self.system_tic = None
        self.plot1.replot()
        self.plot2.replot()

    def moreData(self):
        if self.i == self.count:
            self.i = 0
            self.x1 = random.random()
            self.y1 = random.random()
            self.xs1[self.i] = self.x1
            self.ys1[self.i] = self.y1
            self.x2 = random.random()
            self.y2 = random.random()
            self.xs2[self.i] = self.x2
            self.ys2[self.i] = self.y2
            self.i += 1
            chunks = []
            self.timer_toc = time.time()
            if self.timer_tic:
                chunks.append("wall: %s s." % (self.timer_toc - self.timer_tic))
                print(" ".join(chunks))
            self.timer_tic = self.timer_toc
        else:
            self.x1, self.y1 = standard_map(self.x1, self.y1, self.kappa)
            self.xs1[self.i] = self.x1
            self.ys1[self.i] = self.y1
            self.x2, self.y2 = standard_map(self.x2, self.y2, self.kappa)
            self.xs2[self.i] = self.x2
            self.ys2[self.i] = self.y2
            self.i += 1

    def timerEvent(self, e):
        self.moreData()
        self.curve1.setData(self.xs1[: self.i], self.ys1[: self.i])
        self.curve2.setData(self.xs1[: self.i], self.ys1[: self.i])
        self.plot1.replot()
        self.plot2.replot()


def test_mapdemo():
    """Map demo"""
    utils.test_widget(MapDemo, size=(600, 600))


if __name__ == "__main__":
    test_mapdemo()
