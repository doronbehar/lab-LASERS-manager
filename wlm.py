"""
Module to work with Angstrom WS7 wavelength meter
"""

import sys
import os
import ctypes

import numpy as np

import random

sys.path.append(os.path.dirname(__file__))
from defaults import WLMDATA_PATH, TARGET_FREQS
from constants import UNITSFACTOR

class WLMConnectionError(ConnectionError):
    pass

class WavelengthMeter:
    def __init__(self, dllpath=WLMDATA_PATH):
        """
        Wavelength meter class.
        """
        self.dll = ctypes.WinDLL(dllpath)
        self.dll.GetWavelengthNum.restype = ctypes.c_double
        self.dll.GetFrequencyNum.restype = ctypes.c_double
        self.dll.GetSwitcherMode.restype = ctypes.c_long
        self.dll.SetSwitcherMode(ctypes.c_long(1))
    def GetExposureMode(self):
        return (self.dll.GetExposureMode(ctypes.c_bool(0))==1)
    def SetExposureMode(self, b):
        return self.dll.SetExposureMode(ctypes.c_bool(b))
    def GetWavelength(self, channel=1):
        return self._handleMeasurement(
            self.dll.GetWavelengthNum(ctypes.c_long(channel), ctypes.c_double(0))
        )
    def GetFrequency(self, channel=1):
        return self._handleMeasurement(
            self.dll.GetFrequencyNum(ctypes.c_long(channel), ctypes.c_double(0))
        )
    @staticmethod
    def _handleMeasurement(r):
        # Meaning of negative codes was found online here:
        # https://github.com/RIMS-Code/high_finesse_ws6/blob/1a71e3878208e2d8ebd6c43d9cde623ce13fea04/src/headers/wlmConst.py#L429-L455
        if np.isclose(r,-3):
            return np.nan
        elif np.isclose(r, -5):
            raise WLMConnectionError("Wavelength meter was not found!")
        else:
            return r
    @property
    def switcher_mode(self):
        return self.dll.GetSwitcherMode(ctypes.c_long(0))

class WavelengthMeterShim:
    # randomRange is in KHz
    def __init__(self, randomRange=100):
        self.randomRange = randomRange
    def GetFrequency(self, channel=1):
        # In the real dll channels indices start from 1, so we provide the same
        # experience in this shim function for outside consumers of this class.
        return TARGET_FREQS[channel-1] + \
                random.uniform(-self.randomRange, self.randomRange)
